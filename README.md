# Olmec Games Foundry VTT Packages

This is an empty GitLab repository which is used to host the Foundry VTT packages via GitLab's
in-built [generic package registry](https://docs.gitlab.com/ee/user/packages/generic_packages/).

## Links

The following link can be used to install the "The Drowned Earth Roleplaying Game" in Foundry VTT:

https://gitlab.com/api/v4/projects/29724088/packages/generic/tde-rpg-fvtt-system/metadata/system.json
